package Game;

import java.awt.Button;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.TextField;
import java.awt.TextArea;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class GameFrame extends Frame{
	private
	GameMapPanel MapPanel;
	Label label;
	Button button;
	TextField textField;
	TextArea textArea;

	GameFrame(Karta k) {
		GameWindowListener gwl = new GameWindowListener();
		addWindowListener(gwl);
		
		MapPanel = new GameMapPanel(k);
		MapPanel.setBounds(30,30,90,121);
		this.add(MapPanel);
		button= new Button();
		button.setBounds(150,30,120,100);
		button.setLabel("Кликни");
		this.add(button);
		 textField = new TextField();
		 textField.setBounds(k.sizex*30+60,150,100,30 );
		 textField.setText("Въведи името на героя тук");
		 this.add(textField);
		 
		 textArea= new TextArea();
		 textArea.setBounds(k.sizex*30+60,310,100,90);
		 textArea.setText("Въведи описанието на героя тук");
		 this.add(textArea);
	
	GameButtonActionListener al = new GameButtonActionListener(this);
	button.addActionListener(al);
	
	label = new Label();
	label.setBounds(150,90,100,30);
	label.setText("neshto");
	this.add(label);
	
	this.setSize(300, 300);
	
	}
}